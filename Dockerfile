#
# Dockerfile for running Xen Project on QEMU on ARM64 (aarch64) architecture.
#

# Start from latest Ubuntu 18.04.
FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# Install minimum requirements.
RUN apt-get update && apt-get install -y git wget

# Download Ubuntu server 18.04 Bonic ARM64 image.
COPY bionic-server-cloudimg-arm64.img /bionic-server-cloudimg-arm64.img

# Clone QEMU.
RUN git clone https://github.com/qemu/qemu.git

# Install requirements for building QEMU.
RUN apt-get install -y build-essential
RUN apt-get install -y python3 pkg-config libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev

# Build QEMU System for aarch64/arm64.
RUN cd qemu/                                                && \
    mkdir build                                             && \
    cd build/                                               && \
    ../configure --target-list=arm-softmmu,aarch64-softmmu  && \
    make -j8                                                && \
    make install                                            && \
    cd ../                                                  && \
    rm -rf qemu/

RUN wget https://releases.linaro.org/components/kernel/uefi-linaro/latest/release/qemu64/QEMU_EFI.fd && \
    dd if=/dev/zero of=flash0.img bs=1M count=64 && \
    dd if=QEMU_EFI.fd of=flash0.img conv=notrunc && \
    dd if=/dev/zero of=flash1.img bs=1M count=64
